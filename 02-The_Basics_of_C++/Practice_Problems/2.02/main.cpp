/* 
    Practise Problem
        2.Write a program that displays multiple lines of text onto 
          the screen, each one displaying the name of one of your friends.
*/

#include <iostream>

using namespace std;

int main(){
    cout<<"Russ"<<endl;
    cout<<"Rachel"<<endl;
    cout<<"Monica"<<endl;
    cout<<"Joey"<<endl;
    cout<<"Ursula"<<endl;
}


