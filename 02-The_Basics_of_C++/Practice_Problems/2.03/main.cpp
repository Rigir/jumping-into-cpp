/* Practise Problems
     3. Try commenting out each line of code in the first program we created together and see whether
        the program can compile without it. Look at the errors you get—do they make any sense? Can
        you see why they happened because of the line of code you changed?
*/

//First Program (Origin Version)
      //*
        #include <iostream>

        int main(){
            std::cout << "Hello, world" << std::endl;
        }
      /*/

//I delete include statement | Nothing Happened 
      /*
        int main(){
            std::cout << "Hello, world" << std::endl;
        }
      */

//I delete line with cout object |  Nothing Happened
      /*
        #include <iostream>

        int main(){
          
        }
      */

//I delete in include statement and std:: | error: 'cout' & 'endl' was not declared in this scope -> There is no input/output statements 
      /*
        int main(){
            cout << "Hello, world" << endl;
        }
      */

//I removed evrything except main function | error: ld returned 1 exit status -> Becouse the program always return 1
      /*
        int main(){
        
        }
      */

