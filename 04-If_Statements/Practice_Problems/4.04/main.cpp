/*
    Practice Problem 
        4. Expand the password checking program from earlier in this chapter and make it take multiple usernames, each with
           their own password, and ensure that the right username is used for the right password. Provide the ability 
           to prompt user's again if the first login attempt failed. 
           Think about how easy (or hard) it is to do this for a lot of usernames and passwords. 
            Answer: We need loop !
*/
#include <iostream>
#include <string>

using namespace std;

int main ()
{
	string username;
	string password;
	
    start: // 2. Labeled Statement
    cout << "Enter your username: ";
	getline( cin, username, '\n' );

	cout << "Enter your password: ";
	getline( cin, password, '\n' );


    if ( username == "root" && password == "xyzzy" ){
		cout << "Access allowed \n";
	}
    else if( username == "user" && password == "Le@rn"){
        cout << "Access allowed \n";
    }
	else{
		cout << "Bad username or password. Denied access! \n";
        goto start; // 1. A goto statement provides an unconditional jump from the goto to a labeled statement in the same function. 
	}   //Definition from: tutorialspoint.com. 


	
}
