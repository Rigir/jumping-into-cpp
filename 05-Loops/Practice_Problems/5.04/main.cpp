/*
    Practice Problem
        4. Write a password prompt that gives a user only a certain number of password entry 
           attempts - so that the user cannot easily write a password cracker.
*/

#include <iostream>
#include <string>

using namespace std;

int main(){

    int attempts=0;
    string password;

    while( attempts < 3 ){
        cout<<"\n Please enter your password:  ";
            cin>>password;
        
        if(password != "Q!@w3e"){
            attempts++;

            if(attempts != 3)
                cout<<" Please try again. \n";
            else
                cout<<" Access Denied \n";
        }
        else{
            cout<<" Access Allowed \n";
                break;
        }
    }
   
}

        