/*
    Practice Problem
        5. Try writing each practice problem with each kind of loop—notice which loops work well for each
           kind of problem.
           Answer: While Loop
*/

#include <iostream>

using namespace std;

int main(){

    //Using While Loop
    int BottlesNumber = 100;

    while(BottlesNumber != 0){
        cout<< BottlesNumber <<" bottles of beer on the wall, "<< BottlesNumber <<" bottles of beer! \n";
        BottlesNumber--;

        if(BottlesNumber > 0){
            cout<<"Take one down and pass it around, "<< BottlesNumber <<" bottles of beer on the wall \n"<<endl;
        }
        else{
            cout<<"Take one down and pass it around, no more bottles of beer on the wall. \n";
            cout<<"\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall... \n";
        }
    }

/*
    //Using For Loop
    for(int BottlesNumber = 100; BottlesNumber >= 1;){
        cout<< BottlesNumber <<" bottles of beer on the wall, "<< BottlesNumber <<" bottles of beer! \n";
            BottlesNumber--;
        if(BottlesNumber > 0){
            cout<<"Take one down and pass it around, "<< BottlesNumber <<" bottles of beer on the wall \n"<<endl;
        }
        else{
            cout<<"Take one down and pass it around, no more bottles of beer on the wall. \n";
            cout<<"\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall... \n";
        }
    }
*/

/*
    //Using Do-While Loop
    int BottlesNumber = 100;
    
    do{
        cout<< BottlesNumber <<" bottles of beer on the wall, "<< BottlesNumber <<" bottles of beer! \n";
        BottlesNumber--;

        if(BottlesNumber > 0){
            cout<<"Take one down and pass it around, "<< BottlesNumber <<" bottles of beer on the wall \n"<<endl;
        }
        else{
            cout<<"Take one down and pass it around, no more bottles of beer on the wall. \n";
            cout<<"\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall... \n";
        }
    }while(BottlesNumber != 0);
*/
}