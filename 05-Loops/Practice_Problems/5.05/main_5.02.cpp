/*
    Practice Problem
        5. Try writing each practice problem with each kind of loop—notice which loops work well for each
           kind of problem.
           Answer: Do-While Loop
*/

#include <iostream>

using namespace std;

int main(){

    //Using Do-While Loop
    int menu_choice;

    do{
        cout<<"  ~| Menu |~ \n";
        cout<<" 1. Start Game \n";
        cout<<" 2. Options \n";
        cout<<" 3. Exit \n";

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
    }while(!(menu_choice >= 1 && menu_choice <= 3));

/*
    //Using While Loop
    int menu_choice;
    
    while(!(menu_choice >= 1 && menu_choice <= 3)){
        cout<<"  ~| Menu |~ \n";
        cout<<" 1. Start Game \n";
        cout<<" 2. Options \n";
        cout<<" 3. Exit \n";

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
    }
*/
/*
    //Using For Loop
    int menu_choice;

    for(;;){
        cout<<"  ~| Menu |~ \n";
        cout<<" 1. Start Game \n";
        cout<<" 2. Options \n";
        cout<<" 3. Exit \n";

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;

        if(menu_choice >= 1 && menu_choice <= 3){
            break;
        }
    }
*/
    if (menu_choice == 1){
        cout<<" Let's Rock! \n";
    }
    else if ( menu_choice == 2){
        cout<<" Looking for a resolution ? \n";
    }
    else if(menu_choice == 3){
        cout<<" It's now safe to turn off your computer! \n";
    }
}