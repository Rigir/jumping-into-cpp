/*
    Practice Problem
        5. Try writing each practice problem with each kind of loop—notice which loops work well for each
           kind of problem.
           Answer: Do-While Loop
*/

#include <iostream>

using namespace std;

int main(){
    cout<<"\n This program will sum all your number's ";
    cout<<"\n If you want to finish adding, enter '0' \n\n";

    //Using Do-While Loop
    int number, sum=0;
    do{
        cout<<" Please enter a number: ";
            cin>>number;
        sum+=number;
    }while(number != 0);

/*
    //Using While Loop
    int number=1, sum=0;

    while(number != 0){
        cout<<" Please enter a number: ";
            cin>>number;
        sum+=number;
    }
*/
/*
    //Using For Loop
    int number, sum=0;
    for(;;){
         cout<<" Please enter a number: ";
            cin>>number;
        sum+=number;
        if(number == 0){
            break;
        }
    }
*/
    cout<<"\n Result: "<<sum<<endl;
}