/*
    Practice Problem
        5. Try writing each practice problem with each kind of loop—notice which loops work well for each
           kind of problem.
           Answer: For Loop
*/

#include <iostream>
#include <string>

using namespace std;

int main(){
    int attempts=0;
    string password;

    //Using For Loop
    for(int attempts=0; attempts<3; attempts++){
        cout<<"\n Please enter your password:  ";
            cin>>password;
        
        if(password != "Q!@w3e"){
            if(attempts != 2)
                cout<<" Please try again. \n";
            else
                cout<<" Access Denied \n";   
        }
        else{
            cout<<" Access Allowed \n";
                break;
        }
    }

/*
    //Using While Loop
    while( attempts < 3 ){
        cout<<"\n Please enter your password:  ";
            cin>>password;
        
        if(password != "Q!@w3e"){
            attempts++;

            if(attempts != 3)
                cout<<" Please try again. \n";
            else
                cout<<" Access Denied \n";
        }
        else{
            cout<<" Access Allowed \n";
                break;
        }
    }
*/
/*
    //Using Do-While Loop
    do{
        cout<<"\n Please enter your password:  ";
            cin>>password;
        
        if(password != "Q!@w3e"){
            attempts++;

            if(attempts != 3)
                cout<<" Please try again. \n";
            else
                cout<<" Access Denied \n";
        }
        else{
            cout<<" Access Allowed \n";
                break;
        }
    }while( attempts < 3 );
*/
}