/*
    Practice Problem
        3. Modify your password program from before to put all of the password checking logic into a
           separate function, apart from the rest of the program.
*/

#include <iostream>
#include <string>

using namespace std;

//Function headers 
bool passwordChecker(string password, int attempts);

//The program starts here
int main(){
    string password;

    for(int i=1; i<=3; i++){
        cout<<"\n Please enter your password:  ";
            cin>>password;
        
        if(passwordChecker(password,i))
            break;  
    } 
}

bool passwordChecker(string password, int attempts){
    if(password != "Q!@w3e"){
        if(attempts != 3)
            cout<<" Please try again. \n";
        else
            cout<<" Access Denied \n"; 
        return false;
    }
    else{
        cout<<" Access Allowed \n";
        return true;
    }
}