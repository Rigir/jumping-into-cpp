/*
    Practice Problem
        2. Write a program that picks a number between 1 and 100, and then lets the user guess what the
           number is. The program should tell the user if their guess is too high, too low, or just right.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
void srandSeed();
int randRange(int low, int high);
void check(int number, int hiddenNumber);

//The program starts here!
int main(){
    int hiddenNumber,number,tries;

    srandSeed();
    hiddenNumber = randRange(1,100);

    cout << "\n Guess My Number Game";

    while(!(number == hiddenNumber)){
        cout << "\n Enter a guess between 1 and 100 : ";
        cin>>number;
        check( number, hiddenNumber);
        tries++;
    }
    cout<<"\n Well done, You got it in " << tries << " guesses!\n";
}

void check(int number, int hiddenNumber){  
    if(number > hiddenNumber)
        cout<<" Too high! \n";
    else
        cout<<" Too low! \n";
}

int randRange(int low, int high){
    return rand() % ( high - low + 1) + low;
}

void srandSeed(){
    int seed = time(NULL);
    cout<<" srandSeed: "<<seed<<endl;
    srand(seed);
}