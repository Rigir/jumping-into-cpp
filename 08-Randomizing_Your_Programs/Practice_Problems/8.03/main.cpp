/*
    Practice Problem
        3. Write a program that solves the guessing game from part 2. How many guesses does your program need?
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
void srandSeed();
int randRange(int low, int high);
int JohnTheRipper(int hiddenNumber);

//The program starts here!
int main(){
    int hiddenNumber,number;

    srandSeed();
    hiddenNumber = randRange(1,100);

    cout << "\n Guess My Number Game | JohnTheRipper Edition";

    JohnTheRipper(hiddenNumber); 
}

int JohnTheRipper(int hiddenNumber){ 
    int number = 50, preNumber, min=1, max=100, tries=1;
    
    do{
        tries++;

        //Checking if the number is not the same and the first value is 50
        while(number == preNumber && tries != 1 ) 
            number = randRange(min,max);
        preNumber = number;

        cout<<"\n\n Min: "<< min <<" Max: "<< max <<"\n Number: "<<number;

        //Assigning min and max values
        if( number == hiddenNumber){
            cout<<"\n My program got it in " << tries << " guesses!\n";
        }
        else if( number  > hiddenNumber){
            cout<<"\n Too high! \n"; 
            max = number;
        }
        else if( number  < hiddenNumber){
            cout<<"\n Too low! \n"; 
            min = number;
        }
    }while(!(number == hiddenNumber));
    return number;
}

int randRange(int low, int high){
    return rand() % ( high - low + 1) + low;
}

void srandSeed(){
    int seed = time(NULL);
    cout<<" srandSeed: "<<seed<<endl;
    srand(seed);
}