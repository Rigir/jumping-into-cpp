/*
    Practice Problem
        4. Make a "slot machine" game that randomly displays the results of a slot machine to a player—have 3
           (or more) possible values for each wheel of the slot machine. Don't worry about displaying the text
           "spinning" by. Just choose the results and display them and print out the winnings (choose your own
           winning combinations).
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
void srandSeed();
void displayPoints();
void displaySlotMachine(int slot1, int slot2, int slot3);
int randRange(int low, int high);
int payLine(int slot1, int slot2, int slot3);

//The program starts here!
int main(){
    int slot1, slot2, slot3; 
    char choose;
    srandSeed();

    cout<<"\n One Armed Bandit Slot Machine Game \n";

    do{
        slot1 = randRange(5,7);
        slot2 = randRange(5,7);
        slot3 = randRange(5,7);

        displaySlotMachine(slot1,slot2,slot3);

        cout<<"\n Do you want to play again? ( Y/N ): ";
            cin>>choose;
    }while((choose == 'Y' || choose =='y'));
    
}

void displayPoints(){
    cout<<"\n   _  -| PAY TABLE |-  _ ";
    cout<<"\n   | PAYLINE | POINTS  | ";
    cout<<"\n   |  7 7 7  |   500   | ";
    cout<<"\n   |  6 6 6  |   250   | ";
    cout<<"\n   |  5 5 5  |   100   | ";
}

void displaySlotMachine(int slot1, int slot2, int slot3){
    displayPoints();
    cout<<"\n |-^---------V---------^-|"; 
    cout<<"\n |>| "<< slot1 <<" |---| "<< slot2 <<" |---| "<< slot3 <<" |<|";
    if( payLine(slot1,slot2,slot3) == 0)
        cout<<"\n |-------->  "<< payLine(slot1,slot2,slot3) <<"  <--------| \n";
    else
        cout<<"\n |--------> "<< payLine(slot1,slot2,slot3) <<" <--------| \n";
}

int payLine(int slot1, int slot2, int slot3){
    if(slot1 == 7 && slot2 == 7 && slot3 == 7) 
        return 500;
    else if(slot1 == 6 && slot2 == 6 && slot3 == 6) 
        return 250;
    else if(slot1 == 5 && slot2 == 5 && slot3 == 5) 
        return 100;
    else
        return 0;
}

int randRange(int low, int high){
    return rand() % ( high - low + 1) + low;
}

void srandSeed(){
    int seed = time(NULL);
    cout<<" srandSeed: "<<seed<<endl;
    srand(seed);
}