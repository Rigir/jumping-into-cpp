/*
    Practice Problem
        1. Implement the source code that turns numbers into English text. 
*/

#include <iostream>
#include <string>

using namespace std;

//Function Headers
string translateUnits(int number, string result);
string translateTens(int number, string result);
string translateHund(int number, string result);
string translateThou(int number, string result);

//The program starts here!
int main (){
    int number;
    string result;
    
    do{
        cout<<"Please enter the number you want to translate (from -999,999 to 999,999 ): ";
        cin>>number;
    }while(!(number>=-999999 && number<=999999));

    if( number < 0 ) {
        result += "minus ";
        number = number * -1;
    }

    if( number >= 0 && number < 10){
        cout<<translateUnits(number, result)<<endl;
    }else if( number >= 10 && number < 100){
        cout<<translateTens(number, result)<<endl;
    }else if( number >= 100 && number < 1000){
        cout<<translateHund(number, result)<<endl;
    }else if( number >= 1000 && number < 1000000){
        cout<<translateThou(number, result)<<endl;
    }
}

string translateThou(int number, string result){
    int unit = number / 1000;
    int houd = number % 1000;
    string tempString;
    
    result += translateHund(unit, tempString) + " thousand";

    if(houd != 0) result += ", " + translateHund(houd, tempString);
    return result;
}

string translateHund(int number, string result){
    int unit = number / 100;
    int ten = number % 100;
    string tempString;
    
    if(unit != 0) result += translateUnits(unit, tempString) + " houndred ";
    if(ten >= 10) result += translateTens(ten, tempString);
    else if(ten != 0) result += translateUnits(ten, tempString);

    return result;
}

string translateTens(int number, string result){
    int unit = number % 10;
    string unitString;

    if(number >= 10 && number < 20){
        switch(number){
            case 10: result += "ten"; break;
            case 11: result += "eleven"; break;
            case 12: result += "twelve"; break;
            case 13: result += "thirteen"; break;
            case 14: result += "fourteen"; break;
            case 15: result += "fifteen"; break;
            case 16: result += "sixteen"; break;
            case 17: result += "seventeen"; break;
            case 18: result += "eighteen"; break;
            case 19: result += "nineteen"; break;
        }
    }else if(number >= 20 && number < 30){
       result += "twenty";
    }else if(number >= 30 && number < 40){
        result += "thirty";
    }else if(number >= 40 && number < 50){
        result += "forty";
    }else if(number >= 50 && number < 60){
        result += "fifty";
    }else if(number >= 60 && number < 70){
        result += "sixty";
    }else if(number >= 70 && number < 80){
        result += "seventy";
    }else if(number >= 80 && number < 90){
        result += "eighty";
    }else if(number >= 90 && number < 100){
        result += "ninety";
    }

    if(unit != 0 && number > 19) result += "-" + translateUnits(unit, unitString);
    return result;
}

string translateUnits(int number, string result){
    switch(number){
        case 0: result += "zero"; break;
        case 1: result += "one"; break;
        case 2: result += "two"; break;
        case 3: result += "three";break;
        case 4: result += "four"; break;
        case 5: result += "five"; break;
        case 6: result += "six"; break;
        case 7: result += "seven";break; 
        case 8: result += "eight"; break; 
        case 9: result += "nine"; break;
    }
    return result;
}