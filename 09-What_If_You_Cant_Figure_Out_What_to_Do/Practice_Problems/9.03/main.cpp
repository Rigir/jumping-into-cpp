/*
    Practice Problem
        3. Design a program that finds all numbers from 1 to 1000 whose prime factors, when added
           together, sum up to a prime number (for example, 12 has prime factors of 2, 2, and 3, which
           sum to 7, which is prime). Implement the code for that algorithm.
*/

#include<iostream>

using namespace std;

//Function Headers
int primeFactor (int number);
bool isDivisible (int number, int divisor);
bool isPrime (int number);

//The program starts here!
int main (){
    for ( int i = 1; i <= 1000; i++ ){
		if( isPrime( primeFactor(i))){
			cout<<"Num: "<<i<<" | Sum: "<<primeFactor(i)<<endl;
		}
	} 
}

int primeFactor (int number){
	int k=2,sum=0;
		while(number>1){
			while(isDivisible(number,k)){
				sum+=k;
				number/=k;
			}
			k++;
		}
	return sum; 
}

bool isPrime (int number){
	if(number<=1) return false;
	for ( int i = 2; i < number; i++){
		if ( isDivisible( number, i  )){
			return false;
		}
	}
	return true;
}

bool isDivisible (int number, int divisor){
	return number % divisor == 0;
}