/*
    Practice Problem
        1. Turn the code that we wrote for insertionSort into an insertionSort
           function that works for any sized array.
*/

#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

//Function Heders
int findSmallestRemainingElement (int array[], int size, int index);
void displayArray (int array[], int size);
void swap (int array[], int first_index, int second_index);
void sort (int array[], int size);

//The program starts here!
int main (){
	int size;
	srand( time( NULL ) ); 

    do{
        cout<<" Please enter the array size: ";
            cin>>size;
        if(size <= 0) cout<<" The size of arry must be more then zero! \n";
    }while(size <= 0);

    int array[ size ];

	for ( int i = 0; i < size; i++ ){
		array[ i ] = rand() % 100;
	}

	cout << "Original array: ";
	displayArray( array, size );
	cout << '\n';

	sort( array, size );

	cout << "Sorted array: ";
	displayArray( array, size );
	cout << '\n';
}

void sort (int array[], int size){
	for ( int i = 0; i < size; i++ ){
		int index = findSmallestRemainingElement( array, size, i );
		swap( array, i, index );
	}
}

int findSmallestRemainingElement (int array[], int size, int index){
	int index_of_smallest_value = index;

	for (int i = index + 1; i < size; i++){
		if ( array[ i ] < array[ index_of_smallest_value ] ){
			index_of_smallest_value = i;
		}
	}
	return index_of_smallest_value;
}


void swap (int array[], int first_index, int second_index){
	int temp = array[ first_index ];

	array[ first_index ] = array[ second_index ];
	array[ second_index ] = temp;
}


void displayArray (int array[], int size){
	cout << "{";
	for ( int i = 0; i < size; i++ ){
		if ( i != 0 ){
			cout << ", ";
		}
		cout << array[ i ];
	}
	cout << "}";
}
