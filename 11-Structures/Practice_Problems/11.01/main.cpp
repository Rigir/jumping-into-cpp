/* 
    Practise Problem
        1. Write a program that lets the user fill in a single structure with the name, address, and phone
           number of a single person.
*/

#include <iostream>
#include <string>

using namespace std;

//Structures
struct newPerson{
    string name;
    string address;
    int phoneNumber;
};

//The program starts here
int main(){
    newPerson person;

    cout<<"\n Please, enter your:\n Name: ";
        cin>>person.name;
    cout<<" Address: ";
        cin>>person.address;
    cout<<" Number: ";
        cin>>person.phoneNumber;

    cout<<"\n Hi! "<<person.name<<"! \n Your address: "<<person.address<<"\n Your phone number: "<<person.phoneNumber<<endl;
}

