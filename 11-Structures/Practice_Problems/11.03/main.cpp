/* 
    Practise Problem
        3. Create an address book program that builds on problem #1—this time, the user should be able
           to not just fill out a single structure, but should be able to add new entries, each with a separate
           name and phone number. Let the user add as many entries as he or she wants—is this easy to
           do? It is even possible? Add the ability to display all, or some of the entries, letting the user
           browse the list of entries.
*/

#include <iostream>
#include <string>

using namespace std;

//Structures
struct person{
    string name = " ";
    int phoneNumber;
};

//Function headers
void displayMenu(person addressBook[]);
void addNewPerson(person addressBook[]);
void findPerson(person addressBook[]);

//The program starts here
int main(){
    person addressBook[10];
    int menu_choice;

    while (true){
        displayMenu(addressBook);
        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
         switch(menu_choice){
            case 1:        
                addNewPerson(addressBook);
                break;
            case 2:
                findPerson(addressBook);
                break;
            case 3: 
                cout<<" It's now safe to turn off your computer! \n";
                return true;
            default:            
                cout<<" Error: Bad input! Try again!\n";
                break;
        }
    }
}

void findPerson(person addressBook[]){
    string firstName;
    cout<<"\n Enter a first name of person you looking for: ";
        cin>>firstName;
    
    cout<<" Are you looking for: \n";
    for(int i=0; i<10; i++){
        if(addressBook[i].name == firstName && addressBook[i].name != " "){
            cout<<"  "<<i+1<<"\t "<<addressBook[i].name<<" "<<addressBook[i].phoneNumber<<endl;
        }
    }
    cout<<"\n";
}

void addNewPerson(person addressBook[]){
    for(int i=0; i<10; i++){
        if(addressBook[i].name == " "){
            cout<<"\n Please, enter:\n First Name: ";
                cin>>addressBook[i].name;
            cout<<" Number: ";  
                cin>>addressBook[i].phoneNumber;
            break;
        }
    }
}

void displayMenu(person addressBook[]){
    cout<<"\n\t\t\t  Address Book \n";
    for(int i=0; i<10; i++){
        if(addressBook[i].name != " ")
            cout<<"  "<<i+1<<"\t "<<addressBook[i].name<<" "<<addressBook[i].phoneNumber<<endl;
    }
    cout<<"\n| 1 - Add contact | 2 - Find contact | 3 - Exit |\n";
}
