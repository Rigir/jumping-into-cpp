/* 
    Practise Problem
        4. Write a function that takes two input arguments and provides two separate results to the caller, one
           that is the result of multiplying the two arguments, the other the result of adding them. Since you can
           directly return only one value from a function, you'll need the second value to be returned through a
           pointer or reference parameter.
*/

#include <iostream>

using namespace std;

//Function headers
int multiplyAndSum(int &multi, int number);

//The program starts here
int main(){
    int num1, num2, sum;

    cout<<"\n Please, enter first number: ";
		cin>>num1;
	cout<<" Please, enter second number: ";
        cin>>num2;

    sum = multiplyAndSum( num1, num2);
    cout<<"\n Sum: "<<sum<<" Multiply: "<<num1<<endl;
}

int multiplyAndSum(int &multi, int number){
    int sum = multi + number;
    multi *= number;
    return sum;
}

