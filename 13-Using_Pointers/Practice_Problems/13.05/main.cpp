/* 
    Practise Problem
        5. Write a program that compares the memory addresses of two different variables on the stack and
           prints out the order of the variables by numerical order of their addresses. Does the order surprise you?
*/

#include <iostream>

using namespace std;

//The program starts here
int main(){
    int x = 1, y = 1;
    int *p_num1 = &x, *p_num2 = &y;

    if(p_num1 > p_num2)
        cout<<"1."<<p_num1<<"\n2."<<p_num2<<endl;
    else // This "else" will never be executed because x is declared before y.
        cout<<"2."<<p_num1<<"\n1."<<p_num2<<endl; 
}

