/* 
    Practise Problem
        2. Write a function that takes 3 arguments, a length, width and height, dynamically allocates a 3-
           dimensional array with those values and fills the 3-dimensional array with multiplication tables.
           Make sure to free the array when you are done.
*/

#include <iostream>

using namespace std;

//Function headers
bool isNegative(int length, int width, int height);
void fillArray(int ***p_p_p_array, int length, int width, int height);
void printArray(int ***p_p_p_array, int length, int width, int height);

//The program starts here
int main(){
    int length, width, height;

    cout<<"\n Multiplication Table \n";
    do{
        cout<<" Please, enter length: ";
            cin>>length;
        cout<<" Please, enter width: ";
            cin>>width;
        cout<<" Please, enter height: ";
            cin>>height;
    }while(isNegative(length, width, height));

    // Create a 3D dynamically allocated array.
    int ***p_p_p_multiplicationTab = new int**[height];
    for(int i = 0; i < height; i++){
        p_p_p_multiplicationTab[i] = new int*[width];
        for(int j = 0; j < width; j++){
            p_p_p_multiplicationTab[i][j] = new int[length];
        }
    }
        
    //Here you operate on data.
    fillArray(p_p_p_multiplicationTab, length, width, height);
    printArray(p_p_p_multiplicationTab, length, width, height);

    // Delete a 3D dynamically allocated array.
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++)
            delete []p_p_p_multiplicationTab[i][j];
        delete []p_p_p_multiplicationTab[i];
    } 
    delete []p_p_p_multiplicationTab;
    p_p_p_multiplicationTab = NULL;
}

void fillArray(int ***p_p_p_array, int length, int width, int height){
    for(int i = 0; i < height; i++)
        for(int j = 0; j < width; j++)
            for(int k = 0; k < length; k++)
                p_p_p_array[i][j][k] = (j+1)*(k+1);
}

void printArray(int ***p_p_p_array, int length, int width, int height){
    for(int i = 0; i < height; i++){
        cout<<"\n ===| "<<i+1<<" |==="<<endl;
        for(int j = 0; j < width; j++){
            cout<<"\n -| "<<j+1<<" |-"<<endl;
            for(int k = 0; k < length; k++){
                cout<<" "<<j+1<<" * "<<k+1<<" = "<<p_p_p_array[i][j][k]<<endl;
            }
        }
    }
}

bool isNegative(int length, int width, int height){
    if(length <= 0 || width <= 0 || height <= 0 ){
        cout<<"\n Error: The entered values must be greater then zero \n";
        return true;
    }
    return false;
}


