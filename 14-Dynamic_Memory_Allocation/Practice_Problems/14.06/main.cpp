/* 
    Practise Problem
        6. Write a program that takes a width and a height and dynamically generates a maze with the
           given width and height. The maze must always have a valid path through it (how can you ensure
           this?) Print the maze to the screen once it’s been generated.
*/

#include <iostream>

using namespace std;

//Enums
enum directionsName_t
{
    NORTH = 0,
    SOUTH,
    EAST,
    WEST
};

//Structures
struct direction_t
{
    directionsName_t name;
    int bit;
    int dx;
    int dy;
};

//Global
int width, length;
direction_t dirs[] = {
    {NORTH, 1, 0, -1},
    {SOUTH, 2, 0, 1},
    {EAST, 4, 1, 0},
    {WEST, 8, -1, 0}};

//Function headers
bool isNegative(int width, int length);
void generateMaze(int **p_p_array, int width, int length);
void printArray(int **p_p_array, int width, int length);
void fillArray(int **p_p_array, int width, int length);
int oppositeDirectionBit(directionsName_t name);
void shuffle(direction_t arr[], int size);
bool between(int v, int upper);
void srandSeed();
int randRange(int low, int high);

//The program starts here
int main()
{
    srandSeed();
    do
    {
        cout << " Please, enter the width: ";
        cin >> width;
        cout << " Please, enter the length: ";
        cin >> length;
    } while (isNegative(width, length));

    // Create a 2D dynamically allocated array.
    int **p_p_maze = new int *[width];
    for (int i = 0; i < width; i++)
        p_p_maze[i] = new int[length];

    // Here you operate on data.
    fillArray(p_p_maze, width, length);
    generateMaze(p_p_maze, 0, 0);
    printArray(p_p_maze, width, length);

    // Delete a 2D dynamically allocated array.
    for (int i = 0; i < width; i++)
        delete[] p_p_maze[i];
    delete[] p_p_maze;
    p_p_maze = NULL;
}

void generateMaze(int **p_p_array, int cx, int cy)
{
    direction_t copyOfDirs[4];
    std::copy(std::begin(dirs), std::end(dirs), std::begin(copyOfDirs));
    shuffle(copyOfDirs, 4);
    for (auto dir : copyOfDirs)
    {
        int nx = cx + dir.dx;
        int ny = cy + dir.dy;
        if (between(nx, width) && between(ny, length) && p_p_array[nx][ny] == 0)
        {
            p_p_array[cx][cy] |= dir.bit;
            p_p_array[nx][ny] |= oppositeDirectionBit(dir.name);
            generateMaze(p_p_array, nx, ny);
        }
    }
}

void shuffle(direction_t arr[], int size)
{
    direction_t t;
    int k;
    for (int i = 2; i < size; ++i)
    {
        k = randRange(0, 10) % i;
        t = arr[i - 1];
        arr[i - 1] = arr[k];
        arr[k] = t;
    }
}

bool between(int v, int upper)
{
    return ((v >= 0) && (v < upper));
}

int oppositeDirectionBit(directionsName_t name)
{
    switch (name)
    {
    case NORTH:
        return dirs[SOUTH].bit;
    case SOUTH:
        return dirs[NORTH].bit;
    case EAST:
        return dirs[WEST].bit;
    case WEST:
        return dirs[EAST].bit;
    default:
        return 0;
    }
}

void printArray(int **p_p_array, int width, int length)
{
    for (int i = 0; i < length; i++)
    {
        // draw the north edge
        for (int j = 0; j < width; j++)
        {
            cout << (((p_p_array[j][i] & 1) == 0) ? "+---" : "+   ");
        }
        cout << "+" << endl;
        // draw the west edge
        for (int j = 0; j < width; j++)
        {
            cout << (((p_p_array[j][i] & 8) == 0) ? "|   " : "    ");
        }
        cout << "|" << endl;
    }
    // draw the bottom line
    for (int i = 0; i < width; i++)
    {
        cout << "+---";
    }
    cout << "+" << endl;
}

bool isNegative(int width, int length)
{
    if (width <= 2 || length <= 2)
    {
        cout << "\n Error: The entered values must be greater then two \n";
        return true;
    }
    return false;
}

void fillArray(int **p_p_array, int width, int length)
{
    for (int i = 0; i < width; i++)
        for (int j = 0; j < length; j++)
            p_p_array[i][j] = 0;
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << " srandSeed: " << seed << endl;
    srand(seed);
}