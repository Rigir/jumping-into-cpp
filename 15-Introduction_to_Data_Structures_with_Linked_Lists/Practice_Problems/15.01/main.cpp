/* 
    Practise Problem
        1.Write a program to remove an element from a linked list; the remove function should take just
          the element to be removed. Is this function easy to write—and will it be fast? Could this be
          made easier or faster by adding an additional pointer to the list?
*/

#include <iostream>
#include <ctime>

using namespace std;

//Structures
struct node {
	int number;
	node *p_next;
}; 

//Global variable
node *p_list = NULL;

//Function headers
void srandSeed();
int randRange (int low, int high);
void display(node* p_head);
node *getNewNode(node *p_head);
void deleteNode(int position); 

//The program starts here
int main(){
    srandSeed();
    for (int i = 0; i < 5; i++)
        p_list = getNewNode(p_list);
    
    display(p_list);
    cout<<"\n";
    deleteNode(4);
    display(p_list);

    delete p_list;
    p_list = NULL;
}

void deleteNode(int position){
    if(p_list == NULL){
        cout<<"The linked List is empty! \n";
        return;
    } 

    node *p_temp = p_list;

    if(position == 0){ 
        p_list = p_temp -> p_next; // Change head
        delete p_temp; // Delete old head
        return;
    }

    // Find previous node of the node to be deleted 
    for(int i=0; p_temp != NULL && i<position-1; i++) 
        p_temp = p_temp->p_next; 

    // If position is more than number of nodes 
    if (p_temp == NULL || p_temp->p_next == NULL) {
        cout<<" Position is out of range! \n";
        return;
    }

    // Node temp->next is the node to be deleted 
    // Store pointer to the next of node to be deleted 
    node *next = p_temp->p_next->p_next; 
  
    // Unlink the node from linked list 
    delete(p_temp->p_next);
    p_temp->p_next = next;  // Unlink the deleted node from list     
}

node *getNewNode(node *p_head){
	node *p_temp = new node;
	p_temp -> number = randRange (1, 9);
    p_temp -> p_next = p_head;
	return p_temp;
}

void display(node* p_head){
    while(p_head != NULL){
        cout<< p_head->p_next <<" | "<< p_head->number<<endl;
        p_head = p_head->p_next;
    }
}

int randRange (int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}
