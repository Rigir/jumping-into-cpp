/* 
    Practise Problem
        2. Write a program that adds elements to a linked list in sorted order, rather than at the beginning.
*/

#include <iostream>
#include <ctime>

using namespace std;

//Structures
struct node {
	int number;
	node *p_next;
}; 

//Global variable
node *p_list = NULL;

//Function headers
void srandSeed();
int randRange (int low, int high);
void display(node* p_head);
void sort();
node *getNewNode();
void deleteNode(int position); 


//The program starts here
int main(){
    srandSeed();

    for (int i = 0; i < 5; i++)
        sort();
    
    display(p_list);
   
    delete p_list;
    p_list = NULL;
}

void sort(){
    node *p_new = getNewNode(), *p_temp = p_list;

    if( p_list == NULL || p_list -> number >= p_new -> number){
        p_new -> p_next = p_list;
        p_list = p_new;
    } 
    else{
        while( p_temp -> p_next != NULL && p_temp -> p_next -> number < p_new -> number) { 
            p_temp = p_temp -> p_next;
        } 
        p_new -> p_next = p_temp -> p_next;
        p_temp -> p_next = p_new;
    }
}

node *getNewNode(){
	node *p_temp = new node;
	p_temp -> number = randRange (1, 9);
    p_temp -> p_next = NULL;
	return p_temp;
}

void display(node* p_head){
    while(p_head != NULL){
        cout<< p_head->p_next <<" | "<< p_head->number<<endl;
        p_head = p_head->p_next;
    }
}

int randRange (int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}
