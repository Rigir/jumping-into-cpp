/* 
    Practise Problem
        4. Implement a two player tic-tac-toe game. First use a linked list to represent the board. 
        Then use an array. Which is easier? Why?
*/

/*
    The implementation of the tic-tac-toe game with a blackboard is in Chapter 10, Exercise 4,
    and its implementation was much easier due to the quick access to values. 
*/

#include <iostream>
#include <ctime>

using namespace std;

//Structures
struct node
{
    char field = ' ';
    node *p_next = nullptr;
};

//Global variable
int boardSize = 9;
node *p_boardFields = NULL;

//Function headers
node *getNewNode(node *p_head);

void playersMove(char player);
void displayBoard();
bool draw();
bool winner(char player);
bool checkWinner(char player);
bool isFieldEmpty(int number);
void saveMove(int number, char player);

//The program starts here
int main()
{
    char player;

    //Clear list
    for (int i = 0; i < boardSize; i++)
        p_boardFields = getNewNode(p_boardFields);

    do
    {
        player = (player == 'X') ? 'O' : 'X';
        displayBoard();
        playersMove(player);
    } while (!winner('X') && !winner('O') && !draw());

    delete p_boardFields;
    p_boardFields = NULL;
}

void playersMove(char player)
{
    int move;

    while (!(move >= 1 && move <= 9 && isFieldEmpty(move)))
    {
        cout << "\nPlayer " << player << " Your turn: ";
        cin >> move;

        //error messages
        if (!(move >= 1 && move <= 9))
            cout << "\n Error: This square doesn't exist! Try again! \n";
        else if (!isFieldEmpty(move))
            cout << "\n Error: Your opponent  choose this option before! Try again! \n";
    }
    saveMove(move, player);
}

void saveMove(int move, char player)
{
    node *p_temp = p_boardFields;

    for (int i = 0; i < boardSize; i++)
    {
        if ((move - 1) == i)
        {
            p_temp->field = player;
            break;
        }
        p_temp = p_temp->p_next;
    }
}

bool isFieldEmpty(int number)
{
    node *p_temp = p_boardFields;

    for (int i = 0; i < boardSize; i++)
    {
        if ((number - 1) == i)
            if (p_temp->field == ' ')
                return true;
            else
                return false;
        p_temp = p_temp->p_next;
    }
    return false;
}

bool winner(char player)
{
    if (checkWinner(player))
    {
        displayBoard();
        cout << "\n Player: " << player << " wins! \n\n";
        return true;
    }
    return false;
}

bool checkWinner(char player)
{
    node *p_head = p_boardFields;
    int result = 0;

    //Columns
    p_head = p_boardFields;
    for (int i = 0; i < 3; i++)
    {
        node *p_temp = p_head;
        int count = 0;
        result = 0;
        while (true)
        {
            if (p_temp->field != player)
                break;

            count++;
            if (count > 2)
                return true;

            for (int j = 0; j < 3; j++)
                p_temp = p_temp->p_next;
        }
        p_head = p_head->p_next;
    }

    // //Rows
    p_head = p_boardFields;
    result = 0;
    for (int i = 0; i < boardSize; i++)
    {
        if (p_head->field == player)
        {
            result++;
            if (result >= 3)
                return true;
        }
        if ((i + 1) % 3 == 0)
            result = 0;
        p_head = p_head->p_next;
    }

    //Diagonals
    p_head = p_boardFields;
    for (int i = 4; i >= 2; i -= 2)
    {
        node *p_temp = p_head;
        int count = 0;
        result = 0;
        while (true)
        {
            if (p_temp->field != player)
                break;
            count++;
            if (count > 2)
                return true;
            for (int j = 0; j < i; j++)
                p_temp = p_temp->p_next;
        }
        for (int j = 0; j < 2; j++)
        {
            p_head = p_head->p_next;
        }
    }

    return false;
}

bool draw()
{
    node *p_head = p_boardFields;

    while (p_head != NULL)
    {
        if (p_head->field == ' ')
            return false;
        p_head = p_head->p_next;
    }

    displayBoard();
    cout << "\n    DRAW   \n\n";
    return true;
}

void displayBoard()
{
    node *p_head = p_boardFields;

    cout << "\n";
    for (int i = 1; i <= boardSize; i++)
    {
        cout << " " << p_head->field << " ";
        p_head = p_head->p_next;
        if (i % 3)
            cout << "|";
        else if (i != boardSize)
            cout << "\n---+---+---\n";
        else
            cout << endl;
    }
}

node *getNewNode(node *p_head)
{
    node *p_temp = new node;
    p_temp->field = ' ';
    p_temp->p_next = p_head;
    return p_temp;
}