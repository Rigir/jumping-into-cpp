/* 
    Practise Problem
        1. Write a recursive algorithm to compute the power function pow(x, y) = x^y.
*/

#include <iostream>

using namespace std;

//Function headers
int pow(int x, int y);

//The program starts here
int main(){
    int x,y;

    cout<<"\n Enter the number: ";
        cin>>x;
    cout<<" Enter the power: ";
        cin>>y;
        
    cout<<"\n Answer: "<<pow(x,y)<<"\n";
}

int pow(int x, int y){
    if(y <= 0) return 1;
    return x * pow(x, y-1);
}