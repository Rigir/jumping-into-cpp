/* 
    Practise Problem
        1. Write a program to display the contents of a binary tree. Can you write a program that prints the
           nodes in a binary tree in sorted order? What about in reverse sorted order?
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Structures
struct node{
	int key_value;
	node *p_left;
	node *p_right;
};

//Function headers
void displayTreeASC(node *p_tree);
void displayTreeDESC(node *p_tree);

node* insert(node *p_tree, int key);
void destroyTree(node *p_tree);
int randRange(int low, int high);
void srandSeed();

//The program starts here
int main(){
    srandSeed();
    node *p_root =  NULL;
    
    for(int i=0; i < 5; i++){
        p_root = insert(p_root, randRange(1,10));
    }

    cout<<"\nAscending Order \n";
    displayTreeASC(p_root);
    cout<<"\nDescending Order \n";
    displayTreeDESC(p_root);

    destroyTree(p_root);
    p_root = NULL;
}

void displayTreeASC(node *p_tree){
    if(p_tree != NULL){
        displayTreeASC(p_tree -> p_left);
        cout<<p_tree -> key_value<<endl;
        displayTreeASC(p_tree -> p_right);
    }
}

void displayTreeDESC(node *p_tree){
    if(p_tree != NULL){
        displayTreeDESC(p_tree -> p_right);
        cout<<p_tree -> key_value<<endl;
        displayTreeDESC(p_tree -> p_left);
    }
}

node* insert(node *p_tree, int key){
	if ( p_tree == NULL ){
		node* p_new_tree = new node;
		p_new_tree->p_left = NULL;
		p_new_tree->p_right = NULL;
		p_new_tree->key_value = key;
		return p_new_tree;
	}

	if( key < p_tree->key_value ){
		p_tree->p_left = insert( p_tree->p_left, key );
	}
	else{
		p_tree->p_right = insert( p_tree->p_right, key );
	}
	return p_tree;
}

void destroyTree(node *p_tree){
	if ( p_tree != NULL ){
		destroyTree( p_tree->p_left );
		destroyTree( p_tree->p_right );
		delete p_tree;
 	}
}

int randRange(int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}
