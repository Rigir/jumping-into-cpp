/* 
    Practise Problem
        3. Write a program that checks whether a binary tree is properly balanced.
*/

#include <iostream>
#include <cstdlib> // labs - long absolute value
#include <ctime>

using namespace std;

//Structures
struct node{
	int key_value;
	node *p_left;
	node *p_right;
};

//Function headers
bool isBalanced(node *p_tree);
int height(node* p_tree);
int max(int a, int b);

void displayTree(node *p_tree);
node* insert(node *p_tree, int key);
void destroyTree(node *p_tree);
int randRange(int low, int high);
void srandSeed();

//The program starts here
int main(){
    srandSeed();
    node *p_root =  NULL;
    
    for(int i=0; i < 5; i++){
        p_root = insert(p_root, randRange(1,10));
    }

    cout<<"\nBinary Tree \n";
    displayTree(p_root);

    if(isBalanced(p_root)) cout<<"\nTree is balanced \n"; 
    else cout<<"\nTree is not balanced \n"; 

    destroyTree(p_root);
    p_root = NULL;
}

bool isBalanced(node* p_tree){ 
    if(p_tree == NULL) return 1; 
    int lh = height(p_tree->p_left), rh = height(p_tree->p_right); 
    if( labs(lh - rh) <= 1 && isBalanced(p_tree->p_left) && isBalanced(p_tree->p_right)) return 1; 
    return 0; 
} 
   
int height(node* p_tree){ 
    if(p_tree == NULL) return 0; 
    return 1 + max(height(p_tree->p_left), height(p_tree->p_right)); 
} 

int max(int a, int b) { 
    return (a >= b) ? a : b; 
} 

node* insert(node *p_tree, int key){
	if ( p_tree == NULL ){
		node* p_new_tree = new node;
		p_new_tree->p_left = NULL;
		p_new_tree->p_right = NULL;
		p_new_tree->key_value = key;
		return p_new_tree;
	}

	if( key < p_tree->key_value ){
		p_tree->p_left = insert( p_tree->p_left, key );
	}
	else{
		p_tree->p_right = insert( p_tree->p_right, key );
	}
	return p_tree;
}

void destroyTree(node *p_tree){
	if ( p_tree != NULL ){
		destroyTree( p_tree->p_left );
		destroyTree( p_tree->p_right );
		delete p_tree;
 	}
}

void displayTree(node *p_tree){
    if(p_tree != NULL){
        displayTree(p_tree -> p_left);
        cout<<p_tree -> key_value<<endl;
        displayTree(p_tree -> p_right);
    }
}

int randRange(int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}