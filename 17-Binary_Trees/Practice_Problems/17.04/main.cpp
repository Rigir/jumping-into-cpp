/* 
    Practise Problem
        4. Write a program that checks if the binary tree is properly sorted so that all nodes to the left of a given
           node are less than the value of that node, and all nodes to the right are greater than the value of that
           node.
*/

#include <iostream>
#include <cstdlib> 
#include <ctime>
#include <climits>

using namespace std;

//Structures
struct node{
	int key_value;
	node *p_left;
	node *p_right;
};

//Function headers
int isBST(node *p_tree);
int BSTUtil(struct node* p_tree, int min, int max);

void displayTree(node *p_tree);
node* insert(node *p_tree, int key);
void destroyTree(node *p_tree);
int randRange(int low, int high);
void srandSeed();

//The program starts here
int main(){
    srandSeed();
    node *p_root =  NULL;
    
    for(int i=0; i < 5; i++){
        p_root = insert(p_root, randRange(1,10));
    }

    cout<<"\nBinary Tree \n";
    displayTree(p_root);

    if(isBST(p_root)) cout<<"\n Tree is sorted \n"; 
    else cout<<"\n Tree is not sorted \n"; 


    destroyTree(p_root);
    p_root = NULL;
}

int isBST(node* p_tree) {
   return(BSTUtil(p_tree, INT_MIN, INT_MAX));
}

int BSTUtil(struct node* p_tree, int min, int max){
   if (p_tree==NULL) return 1;
   if (p_tree->key_value < min || p_tree->key_value > max) return 0;
      return BSTUtil(p_tree->p_left, min, p_tree->key_value - 1) && BSTUtil(p_tree->p_right, p_tree->key_value + 1, max);
}

node* insert(node *p_tree, int key){
	if ( p_tree == NULL ){
		node* p_new_tree = new node;
		p_new_tree->p_left = NULL;
		p_new_tree->p_right = NULL;
		p_new_tree->key_value = key;
		return p_new_tree;
	}

	if( key < p_tree->key_value ){
		p_tree->p_left = insert( p_tree->p_left, key );
	}
	else{
		p_tree->p_right = insert( p_tree->p_right, key );
	}
	return p_tree;
}

void destroyTree(node *p_tree){
	if ( p_tree != NULL ){
		destroyTree( p_tree->p_left );
		destroyTree( p_tree->p_right );
		delete p_tree;
 	}
}

void displayTree(node *p_tree){
    if(p_tree != NULL){
        displayTree(p_tree -> p_left);
        cout<<p_tree -> key_value<<endl;
        displayTree(p_tree -> p_right);
    }
}

int randRange(int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}