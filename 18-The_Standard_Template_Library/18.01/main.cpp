/* 
    Practise Problem
        1. Implement a small address  program that allows users to enter names and email addresses,
           remove or change entries, and list the entries in their address . Don’t worry about saving
           the address  to disk; it’s ok to lose the data when the program exits.
*/

#include <iostream>
#include <string>
#include <map>

using namespace std;

//Function headers
void displayMenu(map<string, string> address);
void addNewContact(map<string, string> &address);
void deleteContact(map<string, string> &address);
void changeContact(map<string, string> &address);

//The program starts here
int main(){
    int menu_choice;
    map<string, string> address;

    while(true){
        displayMenu(address);
        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
         switch(menu_choice){
            case 1:        
                addNewContact(address);
                break;
            case 2:
                changeContact(address);
                break;
            case 3:
                deleteContact(address);
                break;
            case 4: 
                cout<<" It's now safe to turn off your computer! \n";
                return true;
            default:            
                cout<<" Error: Bad input! Try again!\n";
                break;
        }
    }
}

void addNewContact(map<string, string> &address){
    string name, email;
    map<string, string>::iterator itr;
    while(true){
        cout<<"\n Please, enter:\n First Name: ";
            cin>>name;
        cout<<" E-mail: ";  
            cin>>email;

        itr = address.find(name);
        if(itr != address.end())
            cout<<" The user: ' " << itr->first << " ' already exist.\n";
        else{
            address[name] = email;
            break;
        }
    }
}

void changeContact(map<string, string> &address){
    string name, email;
    map<string, string>::iterator itr;

    if(address.empty()) cout<<"\n You don't have any contacts! \n";
    else while(true){
        cout<<"\n Looking for:\n First Name: ";
            cin>>name;

        itr = address.find(name);
        if(itr != address.end()){
            address.erase(name);
            cout<<"\n Change the data:\n First Name: ";
                cin>>name;
            cout<<" E-mail: ";  
                cin>>email;
            
            address[name] = email;
            break;
        }else cout<<" The user doesn't exist.\n";
    }
}

void deleteContact(map<string, string> &address){
    string name;
    map<string, string>::iterator itr;

    if(address.empty()) cout<<"\n You don't have any contacts! \n";
    else while(true){
        cout<<"\n Please, enter:\n First Name: ";
            cin>>name;

        itr = address.find(name);
        if(itr != address.end()){
            address.erase(name);
            break;
        }else cout<<" The user doesn't exist.\n";
    }
}

void displayMenu(map<string, string> address){
    cout<<"\n\t\t\t Address Book \n";
    if(address.empty()) cout<<"\t\t The addrees book is empty. \n";
    else for(map<string, string>::iterator itr = address.begin(), end = address.end(); itr != end; ++itr)
        cout<<"N: "<< itr -> first <<" \tE: "<< itr->second <<endl;
    cout<<"\n| 1 - Add contact | 2 - Change contact | 3 - Delete contact | 4 - Exit |\n";
}