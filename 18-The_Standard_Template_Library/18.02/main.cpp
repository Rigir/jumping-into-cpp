/* 
    Practise Problem
        2. Use vectors to implement a high score list for a video game. Make it so that scores are updated
           automatically, and new scores are added into the right place in the list. You may find the SGI
           website listed above useful for finding more operations you can do on vectors.
*/

#include <iostream>
#include <algorithm> // sort
#include <vector>

using namespace std;

//Function headers
void displayMenu(vector<int> scoreBoard);
void addNewScore(vector<int>& scoreBoard);

//The program starts here
int main(){
    int menu_choice;
    vector<int> scoreBoard;

    while(true){
        displayMenu(scoreBoard);
        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
         switch(menu_choice){
            case 1:        
                addNewScore(scoreBoard);
                break;
            case 2: 
                cout<<" It's now safe to turn off your computer! \n";
                return true;
            default:            
                cout<<" Error: Bad input! Try again!\n";
                break;
        }
    }
}

void addNewScore(vector<int>& scoreBoard) {
  int score = 0;
  cout << "Enter you score: ";
    cin >> score; 
  scoreBoard.push_back(score);
  sort(scoreBoard.rbegin(), scoreBoard.rend());
}


void displayMenu(vector<int> scoreBoard){
    cout<<"\n| ---------------------------- |\n";
    if(scoreBoard.empty()) cout<<" The Score board empty. \n";
    else for(vector<int>::iterator itr = scoreBoard.begin(), end = scoreBoard.end(); itr != end; ++itr) 
        cout<<" "<<*itr<<endl;
    cout<<"\n| 1 - Add New Score | 2 - Exit |\n";
}