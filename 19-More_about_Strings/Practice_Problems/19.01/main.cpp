/* 
    Practise Problem
        1. Write a program that reads in two strings, a "needle" and a "haystack" and counts the number
           of times the "needle" appears in the "haystack".
*/

#include <iostream>
#include <string>

using namespace std;

int main(){
    string s1, s2;
    int counter = 0;

    cout<<"The word you are looking for: ";
        getline(cin, s1);
    cout<<"Now enter a sentence or word: ";
        getline(cin, s2);
    for(int i = s2.find(s1,0); i != string::npos; i = s2.find(s1, i)){
        counter++;
        i++;
    }
    cout<<"Word '"<<s1<<"' occurs: "<<counter<<" time/s.";
}

