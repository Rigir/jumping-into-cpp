/* 
    Practise Problem
        2. Write a program that allows a user to type in tabular data similar to a CSV file, but instead of
           using commas a separator, first prompt the user to enter the separator character, then let the
           user type in the lines of tabular data. Provide a set of possible punctuation characters as options
           by looking through the input for non-number, non-letter characters. Find characters that appear
           on every single line, and choose those characters as the option. For example, if you see input
           like this:

           Alex Allain, webmaster@cprogramming.com
           John Smith, john@nowhere.com

           You should prompt the user to choose between comma, at sign, and period for the separator.
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

//The program starts here
int main()
{
    vector<char> separators;
    string data, separator;
    int choice;

    cout << "Enter, data similar to a CSV file: ";
    getline(cin, data);

    for (int i = 0; i < data.length(); i++)
    {
        if (ispunct(data[i]))
        {
            separators.push_back(data[i]);
        }
    }
    cout << "The separators I found: \n";
    for (int i = 0; i < separators.size(); i++)
    {
        cout << i << ". " << separators[i] << " \n";
    }

    if (!separators.empty())
    {
        do
        {
            cout << "\nWhitch separator you would like to choose? \n Enter: ";
            cin >> choice;
        } while (choice < 0 || choice >= separators.size());

        for (int i = 0; i < separators.size(); i++)
            if (choice == i)
                separator = separators[i];

        cout << "The separator you select: " << separator << endl;
    }
    else
        cout << "\nData don't have any separators! \n";
}
