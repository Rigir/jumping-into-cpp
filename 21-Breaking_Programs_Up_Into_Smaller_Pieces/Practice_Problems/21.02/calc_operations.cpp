#include "calc_operations.h"

int addition(const int& x, const int& y){
    return x + y;
}

int subtraction(const int& x, const int& y){
    return x - y;
}

int division(const int& x, const int& y){
    return x / y;
}

int multiplication(const int& x, const int& y){
    return x * y;
}