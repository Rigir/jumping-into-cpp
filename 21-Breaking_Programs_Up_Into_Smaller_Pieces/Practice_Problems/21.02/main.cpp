/* 
    Practise Problem
        2. Take the program you wrote above and put the function definitions into a separate source file
           from the rest of your calculator code.
*/

//#define DEBUG
#include <iostream>
#include "calc_operations.h"

using namespace std;

//The program starts here
int main(){
  char arithmetic_operations;
  int number_one, number_two;

    cout<<"\t ~| Calculator |~  \n";
    cout<<"(Please enter the arithmatic operator): ";
        cin>>arithmetic_operations;

    cout<<"\n Enter first number: ";
        cin>>number_one;
    cout<<" Enter secound number: ";
        cin>>number_two;

    if(arithmetic_operations == '+'){
        cout<<"\n Result: "<< addition(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '-'){
        cout<<"\n Result: "<< subtraction(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '/' || arithmetic_operations == '%'){
        cout<<"\n Result: "<< division(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '*' || arithmetic_operations == 'x'){
        cout<<"\n Result: "<< multiplication(number_one,number_two) <<endl;
    }

    #ifdef DEBUG
        cout <<"\n == BEBUG ===\n Variable n1: "<< number_one <<"\n Variable n1: "<< number_two <<"\n Arithmatic Operator : "<< arithmetic_operations  ;
    #endif
}

