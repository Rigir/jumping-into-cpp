/* 
    Practise Problem
        1. Write a structure that provides the interface to a tic-tac-toe board. Implement a two player tictac-
           toe game with the methods on the structure. You should make it so that basic operations
           like making a move and checking whether one player has won are part of the interface of the
           structure.
*/

#include "tic-tac-toe.h"

//The program starts here
int main(){
    char player;
    TicTacToe game;

    game.clearBoard();
    while(!game.winner('O') && !game.winner('X') && !game.draw()){
        player = ( player == 'X' ) ? 'O' : 'X';
        game.displayBoard();
        game.playersMove(player);
    }
}

