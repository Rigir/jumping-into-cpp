#include <iostream>

using namespace std;

struct TicTacToe{
    char BoardSquers[9];

    void playersMove(char player);
    void displayBoard();
    void clearBoard();
    bool draw();
    bool winner(char player);
    bool checkWinner(char player);
};