/*
    Practise Problem
        1. Implement a sort function that takes a vector of pointers to an
   interface class, Comparable , that defines a method, compare(Comparable&
   other) , and returns 0 if the objects are the same, 1 if the object is
   greater than other , and -1 if the object is less than other . Create a class
   that implements this interface, create several instances, and sort them. If
   you're looking for some inspiration for what to create—try a HighScoreElement
   class that has a name and a score, and sorts so that the top scores are
   first, but if two scores are the same, they are sorted next by name.
*/

#include <iostream>
#include <vector>

#include "Comparable.h"
#include "HighScoreElement.h"

using namespace std;

// Function Heders
void sort(vector<HighScoreElement*>& vec);
void print(vector<HighScoreElement*>& vec);

// The program starts here
int main() {
  vector<HighScoreElement*> vec{
      new HighScoreElement(10, "Monica"), new HighScoreElement(6, "Joey"),
      new HighScoreElement(8, "Ursula"), new HighScoreElement(6, "Tim")};
  sort(vec);
  print(vec);
}

void sort(vector<HighScoreElement*>& vec) {
  for (size_t i = 0; i < vec.size() - 1; ++i) {
    for (size_t j = 0; j < vec.size() - i - 1; ++j) {
      if (vec[i]->compare(*vec[j]) == 1)
        swap(vec.at(j), vec.at(j + 1));
    }
  }
}

void print(vector<HighScoreElement*>& vec) {
  for (auto& i : vec) {
    cout << i->getScore() << " " << i->getName() << endl;
  }
  cout << endl;
}