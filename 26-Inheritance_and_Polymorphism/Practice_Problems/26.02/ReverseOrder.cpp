#include "ReverseOrder.h"

int ReverseOrder::compare(const string& lhs, const string& rhs) {
  if (lhs == rhs)
    return 0;
  else if (lhs > rhs)
    return -1;
  else
    return 1;
}
