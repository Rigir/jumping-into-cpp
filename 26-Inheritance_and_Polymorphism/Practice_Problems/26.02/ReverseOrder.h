#ifndef REVERSEORDER_H
#define REVERSEORDER_H

#include <string>
#include "Comparator.h"

class ReverseOrder : public Comparator {
 public:
  int compare(const string& lhs, const string& rhs);
};

#endif  // REVERSEORDER_H