/*
    Practise Problem
        2. Provide another implementation of your sort function, this time
   taking an interface called Comparator , which has a method compare(const
   string& lhs, const string& rhs) that follows similar rules to the previous
   compare method: return 0 if the two value are the same, 1 if lhs > rhs or -1
   if lhs < rhs . Write two different classes to do comparison: one that does a
   case-insensitive comparison and one that sorts in reverse alphabetical order.
*/

#include <iostream>
#include <string>
#include <vector>

#include "CaseInsensitive.h"
#include "ReverseOrder.h"

using namespace std;

// Function Heders

void sort(vector<string>& vec, Comparator& comparator);
void print(vector<string>& vec);

// The program starts here
int main() {
  CaseInsensitive caseInsensitive;
  ReverseOrder reverseOrder;
  vector<string> vec{"russ", "monica", "Rachel", "Joey", "Ursula"};
  cout << "Sorted with CaseInsensitive: \n";
  sort(vec, dynamic_cast<Comparator&>(caseInsensitive));
  print(vec);
  cout << "Sorted with ReverseOrder: \n";
  sort(vec, dynamic_cast<Comparator&>(reverseOrder));
  print(vec);
}

void sort(vector<string>& vec, Comparator& comparator) {
  for (size_t i = 0; i < vec.size() - 1; ++i) {
    for (size_t j = 0; j < vec.size() - i - 1; ++j) {
      if (comparator.compare(vec[j], vec[j + 1]) == 1)
        swap(vec.at(j), vec.at(j + 1));
    }
  }
}

void print(vector<string>& vec) {
  for (auto& i : vec) {
    cout << i << endl;
  }
}