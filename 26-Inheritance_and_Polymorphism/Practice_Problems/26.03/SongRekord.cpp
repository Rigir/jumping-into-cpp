#include "SongRekord.h"
#include <ctime>

SongRekord::SongRekord(string title, string author, string genre, int length)
    : _title(title), _author(author), _genre(genre), _length(length) {}

string SongRekord::toString() {
  string result;
  result += this->_title + " | " + this->_author + " | " + this->_genre +
            " | " + to_string(this->_length) + "\n";

  time_t now = time(NULL);
  string dt = ctime(&now);
  return result += "Log on: " + dt + "\n";
}