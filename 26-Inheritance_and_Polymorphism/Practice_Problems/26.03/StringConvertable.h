#ifndef STRINGCONVERTABLE_H
#define STRINGCONVERTABLE_H

#include <string>

using namespace std;

class StringConvertable {
 public:
  virtual string toString() = 0;
};

#endif  // STRINGCONVERTABLE_H