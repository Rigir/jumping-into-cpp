/* 
    Practise Problem
        1. Take your implementation of a vector from the practice problem at the end of chapter 25 and
           add it to a namespace.
*/

#include "wektorInt.h"

using namespace vector_int;
using namespace std;

//The program starts here
int main()
{
    cout << "\nTest: Contructor with negative size: \n";
    WektorInt arr(-1);
    cout << "Arr.size:" << arr.getSize() << endl;
    cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

    cout << "Test: Check if pushBack scale size \n\n";
    arr.pushBack(2);
    arr.pushBack(9);
    arr.print();

    arr.pushBack(3);
    arr.print();
    cout << "Arr.size:" << arr.getSize() << endl;
    cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

    cout << "Test: Check if u can set value out of index  \n\n";
    cout << "Positive num:  \n";
    arr.set(5, 15);
    cout << "Negative num:  \n";
    arr.set(-1, 15);
    cout << "Correct (2,15):  \n";
    arr.set(2, 15);
    arr.print();

    cout << "Arr.size:" << arr.getSize() << endl;
    cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

    cout << "Test: Check if pushFront works fine  \n\n";
    arr.pushFront(6);
    arr.print();

    arr.pushFront(8);
    arr.print();

    cout << "Arr.size:" << arr.getSize() << endl;
    cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

    cout << "Test: Check if copy constructor works \n\n";
    cout << "Orginal:  \n";
    arr.print();

    cout << "Copy:  \n";
    WektorInt copy(arr);
    copy.print();

    cout << "Test: Check if copy assignment operator \n\n";
    cout << "Assignment:  \n";
    WektorInt temp = arr;
    temp.print();

    cout << "Orginal:  \n";
    arr.print();
}
