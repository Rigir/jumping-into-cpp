#include "wektorInt.h"

namespace vector_int
{

    WektorInt::WektorInt()
        : _size(32), _numOfElements(0)
    {
        _p_arr = new int[_size]();
    }

    WektorInt::WektorInt(int size)
        : _size(size), _numOfElements(0)
    {
        if (size <= 0)
        {
            std::cout << "\n Error: The size of your array is negative or equals zero! \n I'will set the array size to two \n\n";
            _size = 2;
        }
        _p_arr = new int[_size]();
    }

    WektorInt::~WektorInt()
    {
        delete[] _p_arr;
        _p_arr = NULL;
    }

    WektorInt::WektorInt(const WektorInt &other)
        : _p_arr(nullptr), _size(other._size), _numOfElements(other._numOfElements)
    {
        _p_arr = new int[_size]();
        for (int i = 0; i < _size; i++)
            _p_arr[i] = other._p_arr[i];
    }

    WektorInt &WektorInt::operator=(const WektorInt &other)
    {
        if (this != &other)
        {
            _size = other._size;
            _numOfElements = other._numOfElements;

            //free memory
            delete[] _p_arr;
            _p_arr = NULL;

            _p_arr = new int[_size]();
            for (int i = 0; i < _size; i++)
                _p_arr[i] = other._p_arr[i];
        }
        return *this;
    }

    void WektorInt::pushFront(const int value)
    {
        if (_size <= _numOfElements)
            _p_arr = growArray();

        int *_p_temp = new int[_size];
        _p_temp[0] = value;
        for (int i = 1; i < _size; i++)
            _p_temp[i] = _p_arr[i - 1];
        _p_arr = _p_temp;
        _numOfElements++;
    }

    void WektorInt::pushBack(const int value)
    {
        if (_size <= _numOfElements)
            _p_arr = growArray();
        _p_arr[_numOfElements++] = value;
    }

    void WektorInt::print()
    {
        for (int i = 0; i < _size; i++)
        {
            std::cout << " " << i << ": " << get(i) << std::endl;
        }
        std::cout << std::endl;
    }

    int *WektorInt::growArray()
    {
        _size *= 2;
        int *p_new_array = new int[_size];
        for (int i = 0; i < _size / 2; i++)
            p_new_array[i] = get(i);
        return p_new_array;
    }

    //Setters
    void WektorInt::set(const int index, const int value)
    {
        if (index < 0 || index > _size || index > _numOfElements)
            std::cout << "\n Error: out of range \n\n";
        else
            _p_arr[index] = value;
    }

    //Getters
    int WektorInt::get(const int index)
    {
        return _p_arr[index];
    }

    int WektorInt::getNumOfElements()
    {
        return _numOfElements;
    }

    int WektorInt::getSize()
    {
        return _size;
    }

}