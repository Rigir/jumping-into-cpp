#include <iostream>

namespace vector_int
{
    class WektorInt
    {
    public:
        WektorInt();                                  // constructor
        WektorInt(int size);                          // constructor
        ~WektorInt();                                 // destructor
        WektorInt(const WektorInt &other);            // copy constructor
        WektorInt &operator=(const WektorInt &other); // assignment operator

        void print();
        void pushFront(const int value);
        void pushBack(const int value);
        //Setters
        void set(const int index, const int value);

        //Getters
        int get(const int index);
        int getNumOfElements();
        int getSize();

    private:
        int *_p_arr;
        int _size, _numOfElements;
        int *growArray();
    };
}