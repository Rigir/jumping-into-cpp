/*
    Practise Problem
        1. Write a function that takes a vector and sums all the values in the
   vector, no matter what type of numerical data the vector holds.
*/

#include <iostream>
#include <vector>

using namespace std;

// Function Heders
template <typename T>
T sumOfVector(const vector<T>& vec);

// The program starts here
int main() {
  vector<int> ints{1, 2, 3, 4, 5};
  vector<float> floats{1.1f, 2.2f, 3.3f, 4.4f, 5.5f};
  vector<double> doubles{1.1, 2.2, 3.3, 4.4, 5.5};

  cout << "Ints: " << sumOfVector(ints) << endl;
  cout << "Floats: " << sumOfVector(floats) << endl;
  cout << "Doubles: " << sumOfVector(doubles) << endl;
}

template <typename T>
T sumOfVector(const vector<T>& vec) {
  T sum = T();
  for (auto& element : vec) {
    sum += element;
  }
  return sum;
}
