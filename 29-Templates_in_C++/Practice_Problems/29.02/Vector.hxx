#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

template <typename T>
class Vector {
 public:
  Vector();                                // constructor
  Vector(int size);                        // constructor
  ~Vector();                               // destructor
  Vector(const Vector& other);             // copy constructor
  Vector& operator=(const Vector& other);  // assignment operator

  void print();
  void pushFront(const T value);
  void pushBack(const T value);
  // Setters
  void set(const int index, const T value);

  // Getters
  T get(const int index);
  int getNumOfElements();
  int getSize();

 private:
  T* _p_arr;
  int _size, _numOfElements;
  T* growArray();
};

#endif  // VECTOR_H