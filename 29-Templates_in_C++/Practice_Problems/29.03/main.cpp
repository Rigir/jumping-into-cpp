/*
    Practise Problem
        3. Write a search method that takes a vector, of any type, and a value,
   of any type, and returns true if the value is in the vector, or false if it
   is not.
*/

#include <iostream>
#include <vector>

using namespace std;

// Function Heders
template <typename T, typename S>
bool search(vector<T>& vec, const S& value);
template <typename T>
void print(const vector<T>& vec);

// The program starts here
int main() {
  vector<int> vec{1, 2, 3, 4, 5};

  print(vec);
  cout << "Contains(5): " << search(vec, 5) << endl;
  cout << "Contains(6): " << search(vec, 6) << endl;
  cout << "Contains(1.2f): " << search(vec, 1.2f) << endl;
  cout << "Contains(1.3): " << search(vec, 1.3) << endl;
}

template <typename T, typename S>
bool search(vector<T>& vec, const S& value) {
  for (auto& element : vec)
    if (element == value)
      return true;
  return false;
}

template <typename T>
void print(const vector<T>& vec) {
  for (auto& i : vec) {
    cout << " " << i;
  }
  cout << endl;
}
